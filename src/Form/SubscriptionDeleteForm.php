<?php

namespace Drupal\crm_core_subscriptions\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Subscription entities.
 *
 * @ingroup crm_core_subscriptions
 */
class SubscriptionDeleteForm extends ContentEntityDeleteForm {


}
