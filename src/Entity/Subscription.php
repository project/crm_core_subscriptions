<?php

namespace Drupal\crm_core_subscriptions\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Subscription entity.
 *
 * @ingroup crm_core_subscriptions
 *
 * @ContentEntityType(
 *   id = "crm_core_subscription",
 *   label = @Translation("Subscription"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\crm_core_subscriptions\SubscriptionListBuilder",
 *     "views_data" = "Drupal\crm_core_subscriptions\Entity\SubscriptionViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\crm_core_subscriptions\Form\SubscriptionForm",
 *       "add" = "Drupal\crm_core_subscriptions\Form\SubscriptionForm",
 *       "edit" = "Drupal\crm_core_subscriptions\Form\SubscriptionForm",
 *       "delete" = "Drupal\crm_core_subscriptions\Form\SubscriptionDeleteForm",
 *     },
 *     "access" = "Drupal\crm_core_subscriptions\SubscriptionAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\crm_core_subscriptions\SubscriptionHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "crm_core_subscription",
 *   admin_permission = "administer subscription entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/crm-core/subscription/{subscription}",
 *     "add-form" = "/crm-core/subscription/add",
 *     "edit-form" = "/crm-core/subscription/{subscription}/edit",
 *     "delete-form" = "/crm-core/subscription/{subscription}/delete",
 *     "collection" = "/crm-core/subscription",
 *   },
 *   field_ui_base_route = "crm_core_subscription.settings"
 * )
 */
class Subscription extends ContentEntityBase implements SubscriptionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setActive($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['individual_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Individual by'))
      ->setDescription(t('The individual ID of author of the subscription entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'crm_core_individual')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDescription(t('A boolean indicating whether the subscription is pending or active.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
