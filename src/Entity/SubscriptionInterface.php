<?php

namespace Drupal\crm_core_subscriptions\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Subscription entities.
 *
 * @ingroup crm_core_subscriptions
 */
interface SubscriptionInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the Subscription creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Subscription.
   */
  public function getCreatedTime();

  /**
   * Sets the Subscription creation timestamp.
   *
   * @param int $timestamp
   *   The Subscription creation timestamp.
   *
   * @return \Drupal\crm_core_subscriptions\Entity\SubscriptionInterface
   *   The called Subscription entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * If pending or not.
   *
   * @return bool
   *   TRUE if the Default entity is published.
   */
  public function isActive();

  /**
   * Set active or pending.
   */
  public function setActive($status);

}
